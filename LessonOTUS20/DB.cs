﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20
{
    public class DB
    {
        List<Circle> circles;
        List<Point> points;
        List<Square> squares;
        private static DB instance;

        private DB()
        {
            circles = new List<Circle>()
            {
                new Circle(2,3,5),
                new Circle(5,1,1),
                new Circle(32,51,66),
                new Circle(51,12,11)
            };
            points = new List<Point>
            {
                new Point(65, 21),
                new Point(100, 121)
            };
            squares = new List<Square>
            {
                new Square(20, 60),
                new Square(29, 10)
            };
        }

        public static DB GetInstance()
        {
            if (instance is null)
                instance = new DB();
            return instance;
        }

        public List<Circle> GetCircle()
        {
            return circles;
        }

        public List<Point> GetPoint()
        {
            return points;
        }

        public List<Square> GetSquare()
        {
            return squares;
        }

    }
}
