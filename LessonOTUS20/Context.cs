﻿using LessonOTUS20.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LessonOTUS20.Action;

namespace LessonOTUS20
{
    public class Context
    {
        IVisitor visitor;
        IStrategy strategy;

        public string Action(Action action)
        {
            SetStrategy(action);
            SetVisitor(action);
            var test = strategy.Execute(visitor);
            return test;
        }

        private void SetVisitor(Action action)
        {
            visitor = action.figureType switch
            {
                FigureType.Circle => new VisitorCircle(),
                FigureType.Point => new VisitorPoint(),
                FigureType.Square => new VisitorSquare(),
                _ => null
            };
        }

        private void SetStrategy(Action action)
        {
            strategy = action.opeartion switch
            {
                OperationType.ToJSON => new Strategies.SerializeToJSON(),
                OperationType.ToXML => new Strategies.SerializeToXML(),
                _ => null
            };
        }
    }
}
