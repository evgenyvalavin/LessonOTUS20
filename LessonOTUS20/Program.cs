﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LessonOTUS20.Action;

namespace LessonOTUS20
{
    class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();

            Action r1 = new Action(FigureType.Circle, OperationType.ToJSON);
            Console.WriteLine(context.Action(r1));
            Action r2 = new Action(FigureType.Circle, OperationType.ToXML);
            Console.WriteLine(context.Action(r2));

            Action r3 = new Action(FigureType.Point, OperationType.ToJSON);
            Console.WriteLine(context.Action(r3));

            Action r4 = new Action(FigureType.Square, OperationType.ToJSON);
            Console.WriteLine(context.Action(r4));

            Console.ReadKey();
        }
    }
}
