﻿using LessonOTUS20.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20
{
    public interface IVisitor
    {
        string SerializeToJSON();
        string SerializeToXML();
    }
}
