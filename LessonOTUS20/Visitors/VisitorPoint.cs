﻿using LessonOTUS20.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20.Visitors
{
    public class VisitorPoint : IVisitor
    {
        readonly List<Point> points;

        public VisitorPoint()
        {
            points = DB.GetInstance().GetPoint();
        }

        public string SerializeToJSON()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{\"" + nameof(points) + "\": [");
            foreach (var item in points)
                stringBuilder.Append(SerForAllJ(item));
            stringBuilder.Append("]}");
            return stringBuilder.ToString();
        }

        public string SerializeToXML()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<" + nameof(points) + ">");
            foreach (var circle in points)
            {
                stringBuilder.Append("<" + nameof(points).TrimEnd('s') + ">");
                stringBuilder.Append(SerForAllX(circle));
                stringBuilder.Append("</" + nameof(points).TrimEnd('s') + ">");
            }
            stringBuilder.Append("</" + nameof(points) + ">");
            return stringBuilder.ToString();
        }

        private StringBuilder SerForAllJ(object obj)
        {
            var fields = obj.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            foreach (var field in fields)
                stringBuilder.Append("\"" + field.Name + "\": \"" + field.GetValue(obj) + "\",");
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Append("}");
            return stringBuilder;
        }

        private StringBuilder SerForAllX(object obj)
        {
            var fields = obj.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var field in fields)
                stringBuilder.Append("<" + field.Name + ">" + field.GetValue(obj) + "</" + field.Name + ">");
            return stringBuilder;
        }
    }
}
