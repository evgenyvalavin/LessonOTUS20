﻿using LessonOTUS20.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20.Visitors
{
    public class VisitorCircle : IVisitor
    {
        readonly List<Circle> circles;

        public VisitorCircle()
        {
            circles = DB.GetInstance().GetCircle();
        }

        public string SerializeToJSON()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{\"" + nameof(circles) + "\": [");
            foreach (var item in circles)
                stringBuilder.Append(SerForAllJ(item));
            stringBuilder.Append("]}");
            return stringBuilder.ToString();
        }

        public string SerializeToXML()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<" + nameof(circles) + ">");
            foreach (var circle in circles)
            {
                stringBuilder.Append("<" + nameof(circles).TrimEnd('s') + ">");
                stringBuilder.Append(SerForAllX(circle));
                stringBuilder.Append("</" + nameof(circles).TrimEnd('s') + ">");
            }
            stringBuilder.Append("</" + nameof(circles) + ">");
            return stringBuilder.ToString();
        }

        private StringBuilder SerForAllJ(object obj)
        {
            var fields = obj.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            foreach (var field in fields)
                stringBuilder.Append("\"" + field.Name + "\": \"" + field.GetValue(obj) + "\",");
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Append("}");
            return stringBuilder;
        }

        private StringBuilder SerForAllX(object obj)
        {
            var fields = obj.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var field in fields)
                stringBuilder.Append("<" + field.Name + ">" + field.GetValue(obj) + "</" + field.Name + ">");
            return stringBuilder;
        }
    }
}
