﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20.Strategies
{
    public class SerializeToXML : IStrategy
    {
        public string Execute(IVisitor visitor)
        {
            return visitor.SerializeToXML();
        }
    }
}
