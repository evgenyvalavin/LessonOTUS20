﻿using LessonOTUS20.Strategies;
using LessonOTUS20.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20
{
    public interface IStrategy
    {
        string Execute(IVisitor visitor);
    }
}
