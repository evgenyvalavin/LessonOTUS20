﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS20
{
    public struct Action
    {
        public readonly OperationType opeartion;
        public readonly FigureType figureType;

        public enum OperationType
        {
            ChangePoint, 
            ChangeCircle,
            ChangeSquare,
            ToJSON,
            ToXML
        }

        public enum FigureType
        {
            Circle,
            Point,
            Square
        }

        public Action(FigureType figureType, OperationType opeartion)
        {
            this.opeartion = opeartion;
            this.figureType = figureType;
        }
    }
}
